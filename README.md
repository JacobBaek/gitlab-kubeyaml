# gitlab-kubeyaml
kubernetes yaml files

# components
- namespace
- persistence volume
- persistence volume claim
- deployment
- service

# Following commands
- kubectl create gitlab_namespace.yaml
- kubectl create gitlab_pv.yaml
- kubectl create gitlab_pvc.yaml
- kubectl create gitlab_deploy.yaml
- kubectl create gitlab_svc.yaml
- kubectl get pods -n gitlab

# How to access
If you created pv,pvc, deployment, service, you can access gitlab via localhost.
- http://localhost
